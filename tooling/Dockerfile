FROM ubuntu:bionic

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update
RUN apt-get install -yq \
    bsdmainutils \
    curl \
    gettext \
    python3-pip \
    jq \
    wget \
    unzip

ENV PACKER_VERSION 1.4.5
ENV PACKER_ZIP packer_${PACKER_VERSION}_linux_amd64.zip
RUN wget -O packer https://releases.hashicorp.com/packer/${PACKER_VERSION}/${PACKER_ZIP} \
    && unzip packer -d /usr/local/bin \
    && rm packer

RUN pip3 install -U pip
RUN pip3 install pyyaml==5.1.2 awscli==1.16.290

RUN curl "https://s3.amazonaws.com/session-manager-downloads/plugin/latest/ubuntu_64bit/session-manager-plugin.deb" -o "session-manager-plugin.deb"
RUN dpkg -i session-manager-plugin.deb
COPY docker-entrypoint /usr/bin/
ENTRYPOINT ["docker-entrypoint"]
