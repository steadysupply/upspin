.PHONY: tooling shell ami aws

TOOLING_IMAGE=upspin-tooling

tooling:
	docker build -qt $(TOOLING_IMAGE) tooling

shell: tooling
	$(TOOLING_RUN) bash

ami: tooling
	$(TOOLING_RUN) packer/run build ami

aws: tooling
	$(TOOLING_RUN) aws/run network
	$(TOOLING_RUN) aws/run control
	$(TOOLING_RUN) aws/run compute

aws-cost: tooling
	$(TOOLING_RUN) aws/cost network
	$(TOOLING_RUN) aws/cost control
	$(TOOLING_RUN) aws/cost compute


aws-describe: tooling
	$(TOOLING_RUN) aws/describe network
	$(TOOLING_RUN) aws/describe control
	$(TOOLING_RUN) aws/describe compute

aws-teardown: tooling
	$(TOOLING_RUN) aws/teardown network
	$(TOOLING_RUN) aws/teardown control
	$(TOOLING_RUN) aws/teardown compute

aws-session: tooling
	$(TOOLING_RUN) aws/session

define TOOLING_RUN
	docker run --rm \
		-e AWS_SESSION_BASE64=$(shell tooling/aws-sts-assume-role upspin) \
		-v $(PWD)/tooling:/tooling \
		-v $(PWD)/packer:/packer \
		-v $(PWD)/aws:/aws \
		-it $(TOOLING_IMAGE)
endef
